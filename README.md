# Boot-O-Matic


## Quickstart

> You can listen on a physical interface via "export PHY_NAME=enp2s0f0u8u2" before running make to also listen on a single physical interface

**Setup your host system**
```shell
make setup
```

**Start dnsmasq server**
```shell
make serve
```

**Pull in your EFI loaders**
```shell
make refresh
```

**Start x86_64 qemu to test**
```shell
make qemu-x86_64
```

**Start arm64 qemu to test**
```shell
make qemu-arm64
```


## Starting u-boot from PXE

Start the physical device under test attached to an ethernet interface
specified by "PHY_NAME" on the host.

On the target device running u-boot, execute:
```
run bootcmd_dhcp
```
