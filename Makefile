
TAP_NAME=boottap0
BR_NAME=bootbridge0
PHY_NAME?=
HOST_IP=10.10.10.1
HOST_SUBNET=255.255.255.0
HOST_CIDR=24

DHCP_RANGE="10.10.10.100,10.10.10.120"

BUILDS="/home/kallisti5/Code/haiku"

default:
	@echo "Usage: make (command)"
	@echo ""
	@echo "  valid commands: "
	@echo "    setup    - prepare environment"
	@echo "    clean    - cleanup environment"
	@echo "    serve    - serve images to clients"

banner:
	@echo '         _ '
	@echo '   __   |-|'
	@echo '  [Ll]  |=|'
	@echo '  ====`o"^"'
	@echo ''
	@echo 'Boot-O-Matic'
	@echo ''

refresh:
	rm -rf tftp/loaders
	mkdir -p tftp/loaders
	cp $(BUILDS)/generated.x86_64/objects/haiku/x86_64/release/system/boot/efi/haiku_loader.efi tftp/loaders/bootx64.efi || true
	cp $(BUILDS)/generated.arm64/objects/haiku/arm64/release/system/boot/efi/haiku_loader.efi tftp/loaders/bootaa64.efi || true
	cp $(BUILDS)/generated.riscv64/objects/haiku/riscv64/release/system/boot/efi/haiku_loader.efi tftp/loaders/bootriscv64.efi || true

setup: banner
	@echo "Setting up Boot-O-Matic..."
	sudo ip link add ${BR_NAME} type bridge
	sudo ip link set ${BR_NAME} up
	sudo ip tuntap add ${TAP_NAME} mode tap user $(USER)
	sudo ip link set ${TAP_NAME} up promisc on
	sudo ip link set ${TAP_NAME} master ${BR_NAME}
	@if [ ! -z "${PHY_NAME}" ]; then\
		echo "Adding physical interface ${PHY_NAME} to bootbridge..."; \
		sudo ip address flush dev ${PHY_NAME}; \
		sudo ip link set dev ${PHY_NAME} master ${BR_NAME}; \
		sudo ip link set ${PHY_NAME} up; \
	fi
	sudo ip address add ${HOST_IP}/${HOST_CIDR} brd + dev ${BR_NAME}

clean: banner
	@echo "Tearing down Boot-O-Matic..."
	@if [ -f /tmp/bootomatic-dnsmasq.pid ]; then\
		sudo kill `cat /tmp/bootomatic-dnsmasq.pid`;\
	fi
	@if [ ! -z "${PHY_NAME}" ]; then\
		echo "Removing physical interface ${PHY_NAME} from bootbridge...";\
		sudo ip link delete dev ${PHY_NAME} master ${BR_NAME} || true;\
	fi
	sudo ip link delete dev ${TAP_NAME} master ${BR_NAME} || true
	sudo ip tuntap del ${TAP_NAME} mode tap || true
	sudo ip link delete ${BR_NAME} type bridge || true
	sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i ${TAP_NAME} -o ${BR_NAME} -j ACCEPT

serve:
	# https://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml#processor-architecture
	@if [ -f /tmp/bootomatic-dnsmasq.pid ]; then\
		sudo kill `cat /tmp/bootomatic-dnsmasq.pid`;\
	fi
	sudo dnsmasq --log-facility /tmp/bootomatic-dnsmasq.log --log-queries --log-debug -u root -x /tmp/bootomatic-dnsmasq.pid \
		-i ${BR_NAME} --bind-interfaces -z --dhcp-authoritative --dhcp-range ${DHCP_RANGE} \
		--dhcp-leasefile=/tmp/bootomatic-dnsmasq.lease --enable-tftp --tftp-root `pwd`/tftp,${BR_NAME} \
		--dhcp-option "1,255.255.255.0" --dhcp-option "3,${HOST_IP}" --dhcp-option "6,${HOST_IP},1.1.1.1" \
		--pxe-prompt "Boot-O-Matic" \
		--dhcp-match "set:efi-x86_64,option:client-arch,7" \
		--dhcp-boot "tag:efi-x86_64,loaders/bootx64.efi" \
		--dhcp-match "set:efi-arm32,option:client-arch,10" \
		--dhcp-boot "tag:efi-arm32,loaders/bootarm.efi" \
		--dhcp-match "set:efi-arm64,option:client-arch,11" \
		--dhcp-boot "tag:efi-arm64,loaders/bootaa64.efi" \
		--dhcp-match "set:efi-rv64,option:client-arch,27" \
		--dhcp-boot "tag:efi-rv64,loaders/bootriscv64.efi"

qemu-x86_64:
	sudo qemu-system-x86_64 -m 4G --enable-kvm -device usb-tablet -vga virtio -usb -bios /usr/share/edk2-ovmf/x64/OVMF.fd -boot n \
		-netdev tap,id=bootnet,ifname=${TAP_NAME},script=no,downscript=no -device virtio-net,netdev=bootnet
qemu-arm64:
	sudo qemu-system-aarch64 -cpu cortex-a57 -M virt -m 4G -usb -boot n -bios /usr/share/edk2-ovmf/aarch64/QEMU_CODE.fd \
		-netdev tap,id=bootnet,ifname=${TAP_NAME},script=no,downscript=no -device virtio-net,netdev=bootnet
qemu-riscv64:
	sudo qemu-system-riscv64 -M virt -m 4G -usb -boot n -bios /usr/share/edk2-ovmf/aarch64/QEMU_CODE.fd \
		-netdev tap,id=bootnet,ifname=${TAP_NAME},script=no,downscript=no -device virtio-net,netdev=bootnet
